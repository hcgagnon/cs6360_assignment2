﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraReset : MonoBehaviour
{

    public GameObject cam;
    Vector3 startCam;

    void Start()
    {

        startCam = cam.transform.position; //get original camera position
    }


    void Update()
    {
        if (Input.GetKeyDown("tab"))
        {

            cam.transform.position = startCam;
        }
    }
}
