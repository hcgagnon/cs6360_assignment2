﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Pressing the F Key should flip the user 180 degrees, so that they end up looking behind where they were looking.

public class CameraFlipper : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    
        if (Input.GetKeyDown(KeyCode.F))
        {
            transform.Rotate(Vector3Int.up, 180);
        }
    }
}
