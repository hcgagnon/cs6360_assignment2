﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateStimuli : MonoBehaviour {

    private float startingDistance, dist;
    private Vector3 startingScale;
    private float timer = 0f, timerMax = 0f, timer2 = 0f, timerMax2 = 0f;
    private int counter = 0;
    private bool disappeared = false, visible = false, toggle = false;


    public Camera cam;
    public GameObject redBall, blueBall1, blueBall2;
    public float s1 = 0.65f, s2 = 0.05f;

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            timer = 0f;
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    private bool Waited2(float seconds)
    {
        timerMax2 = seconds;

        timer2 += Time.deltaTime;

        if (timer2 >= timerMax2)
        {
            timer2 = 0f;
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    void Start()
    {

        startingDistance = Vector3.Distance(cam.transform.position, transform.position);
        startingScale = transform.localScale;
        //dist = Vector3.Distance(redBall.transform.position, transform.position);
    }

    void Update()
    {

        //Figure out the current distance by finding the difference from starting distance
        float curDistance = Vector3.Distance(transform.position, cam.transform.position); //- startingDistance; 

        switch (toggle) {
            case false:

                if (((Input.GetKeyDown(KeyCode.S)) /*&& (redBall.GetComponent<Renderer>().enabled == true))*/ || disappeared))
                {

                    Debug.Log(counter);
                    redBall.GetComponent<Renderer>().enabled = false;
                    disappeared = true;
           
                    if (!Waited(2)) return;
                    else
                    {
                        blueBall1.GetComponent<Renderer>().enabled = false;
                        blueBall2.GetComponent<Renderer>().enabled = false;
                        disappeared = false;
                        toggle = true;
                    }
                    transform.localScale = s1 * startingScale + s2 * (startingScale * curDistance);
                }
                break;

            case true:

                if ((Input.GetKeyDown(KeyCode.S)) || visible)
                {

                    redBall.GetComponent<Renderer>().enabled = true;
                    visible = true;
                    disappeared = false;
                    if (!Waited2(2)) return;
                    else
                    {
                        blueBall1.GetComponent<Renderer>().enabled = true;
                        blueBall2.GetComponent<Renderer>().enabled = true;
                        visible = false;
                        toggle = false;
                    }
                    transform.localScale = s1 * startingScale + s2 * (startingScale * curDistance);
                }
                break;
        }
        transform.forward = transform.position - cam.transform.position;
    }
}
