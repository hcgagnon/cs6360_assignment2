﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VR = UnityEngine.VR;
using UnityEngine.XR;



public class ToggleTracking : MonoBehaviour {

    public Camera cam;
    private float counter;
    private bool rotationDisabled = false, positionDisabled = false;
    // Use this for initialization
    void Start () {
        //cam = GetComponentInChildren<Camera>();
    }


    // Update is called once per frame
    void Update() {
        switch (rotationDisabled) {
            case false:

            if ((Input.GetKeyDown(KeyCode.R)))
            {
            OVRManager.useRotationTracking = false;

            rotationDisabled = true;
            }
                break;

            case true:

            if (Input.GetKeyDown(KeyCode.R))
            {
                OVRManager.useRotationTracking = true;
                rotationDisabled = false;

            }
                break;
    }
        switch (positionDisabled)
        {
            case false:

                if ((Input.GetKeyDown(KeyCode.P)))
                {
                    OVRManager.usePositionTracking = false;

                    positionDisabled = true;
                }
                break;

            case true:

                if (Input.GetKeyDown(KeyCode.P))
                {
                    OVRManager.usePositionTracking = true;
                    positionDisabled = false;

                }
                break;

        }   
    }
}
