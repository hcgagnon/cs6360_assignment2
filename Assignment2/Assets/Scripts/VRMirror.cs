﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRMirror : MonoBehaviour {
    public Camera cam;
    public GameObject test;
    public GameObject cube;
    public GameObject mirror;

    private Vector3 d;
    private Vector3 offset;
    private float counter;
    //private Vector3 origin;

	void Start () {
        offset = test.transform.position - cube.transform.position;
        d = cube.transform.position - mirror.transform.position;
        counter = 0;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.M))
            counter += 1;

        if (counter % 2 == 0) //tracking case
        {

            cube.transform.position = test.transform.position - offset;
            cube.transform.rotation = test.transform.rotation;
        }

        if (counter % 2 != 0) //mirror case
        {
            cube.transform.position = new Vector3 (test.transform.position.x - offset.x, test.transform.position.y - offset.y,  d.z - test.transform.position.z + 2f);
            Vector3 Rotation = new Vector3(test.transform.eulerAngles.x, 180f + (-1f * test.transform.eulerAngles.y), (-1f * test.transform.eulerAngles.z));
            cube.transform.localRotation = Quaternion.Euler(Rotation);

           
        }
    }
}
